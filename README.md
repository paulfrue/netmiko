# Netmiko for dummies
*Authors: Thore, Paul, Duy, Vadim*
## In 4 simple steps!
### Step1: Build the Environment.

> **NOTE**: The following Guide will show you how how to use python3. Python2.7 may differ from this Guide.

 ##### On Linux (freshly installed Ubuntu)

Python3:

     $   sudo apt-get install python3 python3-pip
     $   sudo pip3 install netmiko

Python2.7:

     $   sudo apt-get install python2.7 python-pip
     $   sudo python2.7 -m pip install netmiko

 ##### On Windows
 - Download Python3 for Windows [here](https://www.python.org/downloads/windows/)
 - Install Anaconda [here](https://www.anaconda.com/download/), which is an opensource distribution platform that you can install in Windows and other OS's
 - From the Anaconda Shell, run “**conda install paramiko**”.

 ![conda install paramiko](https://1.bp.blogspot.com/-XXLd6koaXr0/W6kcgtspf5I/AAAAAAAABHE/hWKrc2dZ2dYgR9rFXdm3nPEb0RtFG90zgCLcBGAs/s400/anaconda+netmiko+0.JPG)
 - Proceed with "**y**", press enter.

![enter image description here](https://3.bp.blogspot.com/-xKlRrjCEqvk/W6kcg83kQnI/AAAAAAAABHI/YLD0RnCdKvIPCehF9tz8b2sOucdt1PFGgCLcBGAs/s400/anaconda+netmiko.JPG)
 - From the Anaconda Shell, run “**pip install scp**”.
 - Now Install the Git for Windows [here](https://www.git-scm.com/downloads). 
 Git is required for downloading and cloning all the Netmiko library files from Github.
 - From Git Bash window, Clone Netmiko using the following command: 

 `$  git clone https://github.com/ktbyers/netmiko&#8221`

![git clone https://github.com/ktbyers/netmiko&#8221](https://4.bp.blogspot.com/-gJBZfIbTk8w/W6kchfv-9EI/AAAAAAAABHM/kqqsYLeYCy0ApOAK3cEfjekoap0cJhbKgCLcBGAs/s400/netmiko+gitclone.JPG)
 - Once the installation is completed, change the directory to Netmiko using "**cd netmiko**" command.
 - execute **python setup.p&#8203;y install** from Git Bash Window. Once the installation is completed, go to your Windows Command prompt or Anaconda shell and check Netmiko from Python Interpreter shell.

 ![python setup,py install](https://1.bp.blogspot.com/-5ieZItL2VBM/W6kcgnXQM-I/AAAAAAAABHA/tJqwUD6tqhsNYfS5K1qGP0Nyua6uY7EkwCLcBGAs/s400/Netmiko+git+install.JPG)
 - Finally import paramiko and import netmiko, and start using it for python coding.

### Step2: Import netmiko libraries.
![from netmiko import Netmiko](https://i.ibb.co/v3yVtrN/step1.png)
> **NOTE**: If you're using python2.7 use "**from netmiko import ConnectHandler**" instead.

### Step3: Declare an Array with device Parameters.
![enter image description here](https://i.ibb.co/THSqzKc/step2.png)
> **NOTE**: device_types are fixed values. You can find them [here](https://github.com/ktbyers/netmiko/blob/master/netmiko/ssh_dispatcher.py#L75).

> **IMPORTANT NOTE**: The host must be prepared first by defining a management IP address and set up the SSH configuration manually. You may need to execute the "ip domain-name netmiko.demo" command, too. ![enter image description here](https://i.ibb.co/1zpRS9t/ssh.png)

### Step4: Connect to a Device, change to enable mode, send the Command and print the Output.
![enter image description here](https://i.ibb.co/CwFbRhQ/step3.png)
> **NOTE**: If you're using python2.7 use "**net_connect = ConnectHandler(\*\*device)**" instead.

## Full Script
![
](https://i.ibb.co/gDhR1Pn/ganzes.png)

This Demo Script shows us the output of show run command on a cisco device.