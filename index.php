<html>
<head>
	<style>
		@import url('https://fonts.googleapis.com/css?family=Ubuntu+Mono&display=swap');
		body {
			margin: 0px;
			background-color: #202020;
			font-family: 'Ubuntu Mono', monospace;
		}
		#wrapper {
			position: fixed;
			width: 100%;
			height: 100%;
		}
		#left {
			position: relative;
			float: left;
			width: 50%;
			height: 100%;
			left: 0px;
		}
		#right {
			position: relative;
			float: left;
			width: 50%;
			height: 100%;
		}
		textarea {
			width: 90%;
			margin-left: 5%;
			height: 90%;
			margin-top: 5%;
			border-radius: 8px;
			outline: none;
			background-color: #bbbbbb;
		}
		textarea:hover {
			background-color: #eeeeff;
		}
		.select {
    position: relative;
    display: inline-block;
    margin-bottom: 15px;
    width: 100%;
}    .select select {
        font-family: 'Arial';
        display: inline-block;
        width: calc(100% - 20px);
        cursor: pointer;
        padding: 10px 15px;
        outline: 0;
        border: 0px solid #000000;
        border-radius: 8px;
        background: #e6e6e6;
        color: #7b7b7b;
        appearance: none;
        -webkit-appearance: none;
        -moz-appearance: none;
    }
        .select select::-ms-expand {
            display: none;
        }
        .select select:hover,
        .select select:focus {
            color: #000000;
            background: #cccccc;
        }
        .select select:disabled {
            opacity: 0.5;
            pointer-events: none;
        }
.select_arrow {
    position: absolute;
    top: 5px;
    right: 35px;
    width: 8px;
    height: 8px;
    border: solid #7b7b7b;
    border-width: 0 4px 4px 0;
    display: inline-block;
    padding: 3px;
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
}
.select select:hover ~ .select_arrow,
.select select:focus ~ .select_arrow {
    border-color: #000000;
}
.select select:disabled ~ .select_arrow {
    border-top-color: #cccccc;
}
		h2 {
			margin-top: 100px;
			color: #f0f0f0;

		}

		#new {
			position: fixed;
			z-index: 10;
			width: 300px;
			height: 300px;
			background-color: lightslategrey;
			border: 3px solid black;
			left: 50%;
			top: 50%;
			border-radius: 10px;
			transform: translateX(-50%) translateY(-50%);
			padding: 15px;
			display: none;
		}

		#show_if {
			position: absolute;
			z-index: 9;
			width: 90%;
			height: 35%;
			background-color: #151515;
			border: 3px solid #999999;
			left: 0px;
			top: 50%;
			border-radius: 10px;
			padding: 15px;
			padding-top: 30px;
			padding-right: 0px;
			display: block;
			font-size: 10px;
			color: #f0f0f0;
			font-family: 'monospace';
			box-shadow: 0px 0px 15px 2px black;
		} 

		#show_if:before{
			z-index: 11;
			content: 'Interfaces';
			position: absolute;
			width: calc(100% - 10px);
			height: 15px;
			top: 0px;
			left: 0px;
			background-color: #999999;
			border-top-left-radius: 5px;
			border-top-right-radius: 5px;
			color: black;
			padding-left: 10px;
			padding-top: 3px;
		}

		#ajax_show_if {
			position: absolute;
			width: 97%;
			height: calc(94% - 20px);
			left: 3%;
			top: 3%;
			overflow-y: scroll;
			padding-top: 10px;
		}

		#show {
			position: fixed;
			z-index: 10;
			width: 645px;
			height: 550px;
			background-color: #151515;
			border: 3px solid #999999;
			left: 50%;
			top: 50%;
			border-radius: 10px;
			transform: translateY(-50%);
			padding: 15px;
			padding-top: 30px;
			padding-right: 0px;
			display: none;
			font-size: 10px;
			color: #f0f0f0;
			font-family: 'monospace';
			box-shadow: 0px 0px 15px 2px black;
		} 

		#show:before{
			z-index: 11;
			content: 'View';
			position: absolute;
			width: calc(100% - 10px);
			height: 15px;
			top: 0px;
			left: 0px;
			background-color: #999999;
			border-top-left-radius: 5px;
			border-top-right-radius: 5px;
			color: black;
			padding-left: 10px;
			padding-top: 3px;
		}

		#show_hide {
			z-index: 12;
			position: absolute;
			left: 100%;
			margin-left: -18px;
			background-color: red;
			border-radius: 20px;
			width: 15px;
			height: 15px;
			top: 2px;
			font-size: 13px;
			color: #771010;
			font-weight: bold;
			text-align: center;
			cursor: pointer;
		}	

		#ajax_show {
			position: absolute;
			width: 97%;
			height: calc(94% - 20px);
			left: 3%;
			top: 3%;
			overflow-y: scroll;
			padding-top: 10px;
		}

		.button {
			background-color: #552222;
			border-radius: 8px;
			padding: 4px;
			border: 1px solid #303030;
			width: 125px;
			height: 30px;
			cursor: pointer;
			color: white;
			font-size: 15px;
			font-weight: bold;
			outline: none;
		}

		.button:hover{
			background-color: #773333;
		}

		input {
			width: 160px;
			border-radius: 5px;
			outline: none;
			background-color: #cccccc;
		}
		#overlay {
			z-index: 100;
			position: fixed;
			width: 100%;
			height: 100%;
			background-color: #8a0027;
			background-image: url("pattern.png");
			opacity: 0.7;
			display: none;
		}
		#overlay_text {
			position: absolute;
			width: 100%;
			font-size: 50px;
			color: white;
			text-align: center;
			top: 50%;
			transform: translateY(-50%);
		}

/*!
 * Load Awesome v1.1.0 (http://github.danielcardoso.net/load-awesome/)
 * Copyright 2015 Daniel Cardoso <@DanielCardoso>
 * Licensed under MIT
 */
.la-ball-clip-rotate-pulse,
.la-ball-clip-rotate-pulse > div {
    position: relative;
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
}
.la-ball-clip-rotate-pulse {
    display: inline-block;
    font-size: 0;
    color: #fff;
}
.la-ball-clip-rotate-pulse.la-dark {
    color: #333;
}
.la-ball-clip-rotate-pulse > div {
    display: inline-block;
    float: none;
    background-color: currentColor;
    border: 0 solid currentColor;
}
.la-ball-clip-rotate-pulse {
    width: 32px;
    height: 32px;
}
.la-ball-clip-rotate-pulse > div {
    position: absolute;
    top: 50%;
    left: 50%;
    border-radius: 100%;
}
.la-ball-clip-rotate-pulse > div:first-child {
    position: absolute;
    width: 32px;
    height: 32px;
    background: transparent;
    border-style: solid;
    border-width: 2px;
    border-right-color: transparent;
    border-left-color: transparent;
    -webkit-animation: ball-clip-rotate-pulse-rotate 1s cubic-bezier(.09, .57, .49, .9) infinite;
       -moz-animation: ball-clip-rotate-pulse-rotate 1s cubic-bezier(.09, .57, .49, .9) infinite;
         -o-animation: ball-clip-rotate-pulse-rotate 1s cubic-bezier(.09, .57, .49, .9) infinite;
            animation: ball-clip-rotate-pulse-rotate 1s cubic-bezier(.09, .57, .49, .9) infinite;
}
.la-ball-clip-rotate-pulse > div:last-child {
    width: 16px;
    height: 16px;
    -webkit-animation: ball-clip-rotate-pulse-scale 1s cubic-bezier(.09, .57, .49, .9) infinite;
       -moz-animation: ball-clip-rotate-pulse-scale 1s cubic-bezier(.09, .57, .49, .9) infinite;
         -o-animation: ball-clip-rotate-pulse-scale 1s cubic-bezier(.09, .57, .49, .9) infinite;
            animation: ball-clip-rotate-pulse-scale 1s cubic-bezier(.09, .57, .49, .9) infinite;
}
.la-ball-clip-rotate-pulse.la-sm {
    width: 16px;
    height: 16px;
}
.la-ball-clip-rotate-pulse.la-sm > div:first-child {
    width: 16px;
    height: 16px;
    border-width: 1px;
}
.la-ball-clip-rotate-pulse.la-sm > div:last-child {
    width: 8px;
    height: 8px;
}
.la-ball-clip-rotate-pulse.la-2x {
    width: 64px;
    height: 64px;
}
.la-ball-clip-rotate-pulse.la-2x > div:first-child {
    width: 64px;
    height: 64px;
    border-width: 4px;
}
.la-ball-clip-rotate-pulse.la-2x > div:last-child {
    width: 32px;
    height: 32px;
}
.la-ball-clip-rotate-pulse.la-3x {
    width: 96px;
    height: 96px;
}
.la-ball-clip-rotate-pulse.la-3x > div:first-child {
    width: 96px;
    height: 96px;
    border-width: 6px;
}
.la-ball-clip-rotate-pulse.la-3x > div:last-child {
    width: 48px;
    height: 48px;
}
/*
 * Animations
 */
@-webkit-keyframes ball-clip-rotate-pulse-rotate {
    0% {
        -webkit-transform: translate(-50%, -50%) rotate(0deg);
                transform: translate(-50%, -50%) rotate(0deg);
    }
    50% {
        -webkit-transform: translate(-50%, -50%) rotate(180deg);
                transform: translate(-50%, -50%) rotate(180deg);
    }
    100% {
        -webkit-transform: translate(-50%, -50%) rotate(360deg);
                transform: translate(-50%, -50%) rotate(360deg);
    }
}
@-moz-keyframes ball-clip-rotate-pulse-rotate {
    0% {
        -moz-transform: translate(-50%, -50%) rotate(0deg);
             transform: translate(-50%, -50%) rotate(0deg);
    }
    50% {
        -moz-transform: translate(-50%, -50%) rotate(180deg);
             transform: translate(-50%, -50%) rotate(180deg);
    }
    100% {
        -moz-transform: translate(-50%, -50%) rotate(360deg);
             transform: translate(-50%, -50%) rotate(360deg);
    }
}
@-o-keyframes ball-clip-rotate-pulse-rotate {
    0% {
        -o-transform: translate(-50%, -50%) rotate(0deg);
           transform: translate(-50%, -50%) rotate(0deg);
    }
    50% {
        -o-transform: translate(-50%, -50%) rotate(180deg);
           transform: translate(-50%, -50%) rotate(180deg);
    }
    100% {
        -o-transform: translate(-50%, -50%) rotate(360deg);
           transform: translate(-50%, -50%) rotate(360deg);
    }
}
@keyframes ball-clip-rotate-pulse-rotate {
    0% {
        -webkit-transform: translate(-50%, -50%) rotate(0deg);
           -moz-transform: translate(-50%, -50%) rotate(0deg);
             -o-transform: translate(-50%, -50%) rotate(0deg);
                transform: translate(-50%, -50%) rotate(0deg);
    }
    50% {
        -webkit-transform: translate(-50%, -50%) rotate(180deg);
           -moz-transform: translate(-50%, -50%) rotate(180deg);
             -o-transform: translate(-50%, -50%) rotate(180deg);
                transform: translate(-50%, -50%) rotate(180deg);
    }
    100% {
        -webkit-transform: translate(-50%, -50%) rotate(360deg);
           -moz-transform: translate(-50%, -50%) rotate(360deg);
             -o-transform: translate(-50%, -50%) rotate(360deg);
                transform: translate(-50%, -50%) rotate(360deg);
    }
}
@-webkit-keyframes ball-clip-rotate-pulse-scale {
    0%,
    100% {
        opacity: 1;
        -webkit-transform: translate(-50%, -50%) scale(1);
                transform: translate(-50%, -50%) scale(1);
    }
    30% {
        opacity: .3;
        -webkit-transform: translate(-50%, -50%) scale(.15);
                transform: translate(-50%, -50%) scale(.15);
    }
}
@-moz-keyframes ball-clip-rotate-pulse-scale {
    0%,
    100% {
        opacity: 1;
        -moz-transform: translate(-50%, -50%) scale(1);
             transform: translate(-50%, -50%) scale(1);
    }
    30% {
        opacity: .3;
        -moz-transform: translate(-50%, -50%) scale(.15);
             transform: translate(-50%, -50%) scale(.15);
    }
}
@-o-keyframes ball-clip-rotate-pulse-scale {
    0%,
    100% {
        opacity: 1;
        -o-transform: translate(-50%, -50%) scale(1);
           transform: translate(-50%, -50%) scale(1);
    }
    30% {
        opacity: .3;
        -o-transform: translate(-50%, -50%) scale(.15);
           transform: translate(-50%, -50%) scale(.15);
    }
}
@keyframes ball-clip-rotate-pulse-scale {
    0%,
    100% {
        opacity: 1;
        -webkit-transform: translate(-50%, -50%) scale(1);
           -moz-transform: translate(-50%, -50%) scale(1);
             -o-transform: translate(-50%, -50%) scale(1);
                transform: translate(-50%, -50%) scale(1);
    }
    30% {
        opacity: .3;
        -webkit-transform: translate(-50%, -50%) scale(.15);
           -moz-transform: translate(-50%, -50%) scale(.15);
             -o-transform: translate(-50%, -50%) scale(.15);
                transform: translate(-50%, -50%) scale(.15);
    }
}
	</style>
	<script>
var ajaxObj = null;
    ajaxObj = new XMLHttpRequest();

var ajaxObjif = null;
    ajaxObjif = new XMLHttpRequest();

function viewhost() {
    if (ajaxObj.readyState == 4) {
        var message = ajaxObj.responseText;
    }
    if(message != undefined){
        document.getElementById("ajax_show").innerHTML = message;
    }
}

function viewhost_if() {
    if (ajaxObjif.readyState == 4) {
        var message = ajaxObjif.responseText;
    }
    if(message != undefined){
        document.getElementById("ajax_show_if").innerHTML = message;
    }
}

function expandview() {
	document.getElementById("ajax_show").innerHTML = "Retrieving Data... Please wait";
	document.getElementById("show").setAttribute("style", "display: block;");
}

function expandview_if() {
	document.getElementById("ajax_show_if").innerHTML = "Retrieving Interfaces... Please wait";
}

function ajax(){
	elem = document.getElementById("chooser");
	ident = elem.value;
    ajaxObj.open("GET", "showrun.php?ID="+ident);
    ajaxObj.onreadystatechange = viewhost;
    ajaxObj.send(null);
}

function ajax_if(){
	elem = document.getElementById("chooser");
	ident = elem.value;
    ajaxObjif.open("GET", "showif.php?ID="+ident);
    ajaxObjif.onreadystatechange = viewhost_if;
    ajaxObjif.send(null);
}
		
function newhost(){
	document.getElementById("new").setAttribute("style", "display: block;");
}

function show_hide(){
	document.getElementById("show").setAttribute("style", "display: none;");
}

function loader(){
	document.getElementById("overlay").setAttribute("style", "display: block;");
}
	</script>
</head>

<?php
include('sql.php');
?>

<body>
	<div id="wrapper">
		<div id="overlay">
			<div id="overlay_text">
				Write Config... 
				<div class="la-ball-clip-rotate-pulse la-3x">
    				<div></div>
    				<div></div>
				</div>
			</div>
		</div>
		<div id="show">
			<div id="show_hide" onclick="show_hide();">x</div>
			<div id="ajax_show"></div>
		</div>
		<div id="new">
			<h1>
				<font color="white">Create New Device</font>
			</h1>
			<hr>
<form method="post" action="createnew.php">
			Device Type:&nbsp;<input type="text" name="device_type"><br>
			Hostname/IP:&nbsp;<input type="text" name="host"><br>
			Username:&nbsp;<input type="text" name="username"><br>
			Login Password:&nbsp;<input type="password" name="sshpass"><br>
			Enable Password:&nbsp;<input type="password" name="enpass"><br>
			Description:&nbsp;<input type="text" name="description"><br>
			<input class="button" type="submit" name="new" value="Save">
</form>
		</div>
<form method="post" action="writeconf.php">
		<div id="left">
			<textarea name="config" placeholder="Place Config here."></textarea>
		</div>
		<div id="right">
			<div id="show_if">
				<div id="ajax_show_if"></div>
			</div>
			<h2>Choose Device</h2>
			<div class="select">
				<select name="device" id="chooser" onchange="ajax_if(); expandview_if();">
					<?php
						$query = "SELECT ID,host,description FROM netmiko";
						$result = $mysqli->query ($query);
						                
						$i = 0;
						while ($row = $result->fetch_assoc()) {
							if($i<$result->num_rows) {
						        $ID[$i] = $row["ID"];
						        $host[$i] = $row["host"];
						        $description[$i] = $row["description"];
						                            
						        $i++;
						    }
						}
						for ($i = 0; $i<sizeof($ID); $i++){
 							echo "<option value=\"".$ID[$i]."\">".$description[$i]." | ".$host[$i]."</option>";
						}
					?>
				</select>
				<div class="select_arrow"></div>
			</div>
			<input class="button" type="button" value="New" onclick="newhost()">
			<input class="button" type="button" value="View" onclick="ajax(); expandview();">
			<input class="button" type="submit" value="Apply" onclick="loader();">
		</div>
</form>
	</div>
</body>
</html>