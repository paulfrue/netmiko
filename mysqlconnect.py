import mysql.connector
from mysql.connector import errorcode
import settings

class mysqlconnect:

    def __init__(self):
        self.hostname = settings.DB_HOSTNAME
        self.username = settings.DB_USERNAME
        self.password = settings.DB_PASSWORD
        self.port = settings.DB_PORT
        self.database = settings.DB_DATABASE

        try:
            self.mysql = mysql.connector.connect(user=self.username,
                                                 password=self.password,
                                                 host=self.hostname,
                                                 port=self.port,
                                                 db=self.database)
            
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Access on Database DENIED.")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database "+self.database+" does not exist.")
            else:
                print("Undefined Database Error.")
            exit()


    def countrow(self, query):
        self.cursor = self.mysql.cursor(buffered=True, dictionary=True)
        self.cursor.execute(query)
        result = self.cursor.rowcount
        return result

    def sendquery(self, query):
        self.cursor = self.mysql.cursor(buffered=True, dictionary=True)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    def sendinsert(self, query):
        self.cursor = self.mysql.cursor(buffered=True, dictionary=True)
        self.cursor.execute(query)

    def getAllHosts(self):
        query = "SELECT * FROM remotehosts"
        self.cursor = self.mysql.cursor(buffered=True, dictionary=True)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result

    def closeconnection(self):
        self.mysql.commit()
        self.mysql.close()