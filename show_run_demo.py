from netmiko import Netmiko

switch = {
			'device_type': 'cisco_ios',
			'host': '192.168.122.100',
			'username': 'admin',
			'password': '1234QWer',
			'secret' : 'cisco'
		}

net_connect = Netmiko(**switch)
net_connect.enable()
output = net_connect.send_command("show run")
net_connect.disconnect()

print(output)

exit()

