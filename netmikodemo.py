#!/usr/bin/python2.7
from netmiko import ConnectHandler
from sys import argv
import warnings
import mysqlconnect

class netmikodemo():

	def __init__(self):

		# Ignore the Paramiko Deprecation Warnings
		warnings.filterwarnings(action='ignore',module='.*paramiko.*')

		# Define each device
		mysql = mysqlconnect.mysqlconnect()
		self.allDevices = mysql.sendquery("SELECT device_type,host,username,password,enable FROM netmiko")
		mysql.closeconnection()

		if "-h" in argv:
			print("================================ HELP ==================================")
			print("netmikodemo -h                                            | Displays this help text.")
			print("netmikodemo -c device_type ip user pass secret COMMAND    | Executes command on defined device.")

		if "-c" in argv:

			# Extract the CLI Command out of the givven Arguments
			size = len(argv)
			iterator = 7
			cmd = ""
			while size>iterator:
				cmd = str(cmd)+str(argv[iterator])+" "
				iterator = iterator + 1

			# Send the Command to cmdToDevice function
			self.cmdToDevice(cmd)
			exit()

		if "-s" in argv:
			self.showrun()
			exit()

		if "-i" in argv:
			self.showif()
			exit()

		if "-d" in argv:
			self.showdomain()
			exit()

		if "-w" in argv:
			self.configwrite("config.temp")
			exit()

	def showrun(self):
		machine = {
			'device_type' : argv[2],
			'host' : argv[3],
			'username' : argv[4],
			'password' : argv[5],
			'secret' : argv[6],
		}

		try:
			net_connect = ConnectHandler(timeout=10, **machine)
			if machine['device_type'] == "cisco_ios":
				output = net_connect.enable()
				output = net_connect.send_command_expect("conf t", expect_string=r'#')
				output = net_connect.send_command_expect("do show run")
			if machine['device_type'] == "linux":
				output = net_connect.send_command("pstree")
			print(output.encode('utf-8', errors='ignore'))
			net_connect.disconnect()
		except Exception:
			print('<font color=\"red\">>> SSH Failed <<</font>\nVerify that the device is up and operational.')

		exit()

	def showif(self):
		machine = {
			'device_type' : argv[2],
			'host' : argv[3],
			'username' : argv[4],
			'password' : argv[5],
			'secret' : argv[6],
		}

		try:
			net_connect = ConnectHandler(timeout=10, **machine)
			if machine['device_type'] == "cisco_ios":
				output = net_connect.send_command("show ip int br")
			if machine['device_type'] == "linux":
				output = net_connect.send_command("ifconfig")
			print(output.encode('utf-8', errors='ignore'))
			net_connect.disconnect()
		except Exception:
			print('<font color=\"red\">>> SSH Failed <<</font>\nVerify that the device is up and operational.')
		exit()

	def configwrite(self, cfg_file):

		# Read the config file and print out
		load = open(cfg_file, "r")
		cfg_out = load.read()
		load.close()
		print("<font color=\"red\">----------------- CONFIG ----------------</font>\n\n"+cfg_out)

		machine = {
			'device_type' : argv[2],
			'host' : argv[3],
			'username' : argv[4],
			'password' : argv[5],
			'secret' : argv[6],
		}

		try:
			net_connect = ConnectHandler(**machine)
			if machine["device_type"] == 'cisco_ios':
				net_connect.enable()
				net_connect.send_command_expect("conf t", expect_string=r'#')
			output = net_connect.send_config_from_file(cfg_file)
			net_connect.disconnect()
			print("\n\n\n<font color=\"red\">----------------- OUTPUT ----------------</font>\n\n"+output.encode('utf-8', errors='ignore'))
		except Exception:
			print('<font color=\"red\">>> SSH Failed <<</font>\nVerify that the device is up and operational.')
		exit()

	def showdomain(self):
		machine = {
			'device_type' : argv[2],
			'host' : argv[3],
			'username' : argv[4],
			'password' : argv[5],
			'secret' : argv[6],
		}

		try:
			net_connect = ConnectHandler(timeout=10, **machine)
			if machine['device_type'] == "cisco_ios":
				net_connect.enable()
				net_connect.send_command_expect("conf t", expect_string=r'#')
				output = net_connect.send_command_expect("do show run | incl ip domain-name")
			if machine['device_type'] == "linux":
				output = net_connect.send_command("whoami")
			print(output.encode('utf-8', errors='ignore'))
			net_connect.disconnect()
		except Exception:
			print('<font color=\"red\">>> SSH Failed <<</font>\nVerify that the device is up and operational.')
		exit()

	def cmdToDevice(self, cmd):

		machine = {
			'device_type' : argv[2],
			'host' : argv[3],
			'username' : argv[4],
			'password' : argv[5],
			'secret' : argv[6],
		}

		# Prints out the givven command
		print("\n\033[94m\033[4mCommand:\033[0m\n"+cmd)

		# Connects to the device, executes command, disconnects and prints output
		try:
			net_connect = ConnectHandler(**machine)
			if machine['device_type'] == 'cisco_ios':
				net_connect.enable()
				net_connect.send_command_expect("conf t", expect_string=r'#')
			output = net_connect.send_command(cmd)
			net_connect.disconnect()
			print("\n\033[94m\033[4m"+machine['host']+" -> Output:\033[0m\n"+output)
		except Exception as err:
			print(err)
			print('\n\033[94m\033[4m>> SSH Failed <<\033[0m\nVerify that the device is up and operational.')
		exit()

# Start the script
netmikodemo()


